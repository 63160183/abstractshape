/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.abstractshape;

/**
 *
 * @author ACER
 */
public class TestShape {
    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.2);
        Square sq1 = new Square(2.0);
        Square sq2 = new Square(4.0);
        Rectangle rec1 = new Rectangle(3,2);
        Rectangle rec2 = new Rectangle(4,3);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println("------------");
        System.out.println(sq1);
        System.out.println(sq2);
        System.out.println("------------");
        System.out.println(rec1);
        System.out.println(rec2);
        System.out.println("------------");
        
        Shape[] shapes = {c1,c2,c3,sq1,sq2,rec1,rec2};
        for(int i=0;i<shapes.length;i++){
            System.out.println(shapes[i].getName()+" area: "+ shapes[i].calArea());
        }
    }
}
